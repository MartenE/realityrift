﻿using UnityEngine;
using System.Collections;

public class WebcamTexture : MonoBehaviour {

//	 WebCamTexture texture;
	public CanvasRenderer rend;
	public GUITexture BackgroundTexture;
	public WebCamTexture webcamTexture;

	// Use this for initialization
	void Start () {

		WebCamDevice[] devices = WebCamTexture.devices;

		BackgroundTexture = GetComponent<GUITexture>();

//		rend = this.gameObject.GetComponent<CanvasRenderer> ();
		BackgroundTexture.pixelInset =  new Rect(0,0,0,0);  


		webcamTexture = new WebCamTexture(0,0,0);

//		for (var i = 0; i <= devices.Length; i++) {
//			print (devices[i-1]);
//		}

		webcamTexture.deviceName = devices[1].name;

		print (webcamTexture.deviceName);

		BackgroundTexture.texture = webcamTexture;

		webcamTexture.Play();





	}
	
	// Update is called once per frame
	void Update () {

	}
}
