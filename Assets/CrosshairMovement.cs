﻿using UnityEngine;
using System.Collections;

public class CrosshairMovement : MonoBehaviour {

	CapsuleCollider coll;
	float speed;
	GameObject enemy;
//	Light light;
	// Use this for initialization
	void Start () {
		speed  = 2.0f;
//		light = GetComponent<Light> ();

		enemy = GameObject.Find("AR Sprite");
		enemy.transform.position = new Vector3 (-1000, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
		transform.position += move * speed * Time.deltaTime;

		if (Input.GetKeyDown (KeyCode.F5)) {
			print (enemy);
			Instantiate(enemy, new Vector3(-23, 3,-7), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			StartCoroutine(shoot());
		}

	}

	IEnumerator shoot(){
		print ("BANG!");
//		light.enabled = true;
		yield return new WaitForSeconds(1);
//		light.enabled = false;
	}

	void OnCollisionEnter(Collision collider){
		print ("Enter");
	}

	void OnCollisionStay(Collision collision){
//		print("Collision");
		if (Input.GetKeyDown (KeyCode.Space)) {
			Destroy(collision.gameObject);
		}
	}

}
