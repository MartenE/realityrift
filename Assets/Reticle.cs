﻿using UnityEngine;
using System.Collections;

public class Reticle : MonoBehaviour {

	public Camera CameraFacing;
	CapsuleCollider coll;
	GameObject og;
	GameObject enemy;
	bool canShoot;
	bool colliding;
	public bool stopSpawning;
	Collision collision2;
	AudioSource source;

	// Use this for initialization
	void Start () {
		
		og = GameObject.Find("Enemy");
		og.transform.position = new Vector3 (-1000, 0, 0);
		StartCoroutine(spawn());
		canShoot = true;
		stopSpawning = false;
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (CameraFacing.transform.position);
		transform.Rotate (0f, 180f, 0f);
		transform.position = CameraFacing.transform.position +
		CameraFacing.transform.rotation * Vector3.forward * 2.0f;

//		var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
//		transform.position += move * 2.0f * Time.deltaTime;


		if (Input.GetKeyDown (KeyCode.F5)) {
			print (enemy);
			GameObject newEnemy = (GameObject)Instantiate(enemy, new Vector3(-20, -3, 0), Quaternion.identity);
			newEnemy.AddComponent<EnemyState>();
		}
		
		if ((Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0)) && canShoot ) {
			StartCoroutine(shoot());
		}

		if (stopSpawning)
			Destroy (this.gameObject);
	}
	IEnumerator shoot(){
		RaycastHit hit;

//		GameObject enemy = collision2.gameObject;
		Ray ray = new Ray (transform.position, Vector3.forward);

		if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
			if(hit.collider.tag == "Enemy"){
				enemy = hit.collider.gameObject;
				EnemyState enemyState = enemy.GetComponent<EnemyState> ();
				enemyState.health -= 1;
				GameObject.Find("GunSound").GetComponent<AudioSource>().Play();
				canShoot = false;
				if(enemyState.health <= 0){
					AudioClip deathSound = Resources.Load("Sound/zombieDeath", typeof(AudioClip))as AudioClip;
					source.clip = deathSound;
					source.Play();
				}

				yield return new WaitForSeconds (1);
				//light.enabled = false;
				canShoot = true;
			}
		}
	}

	IEnumerator spawn(){
		for (var i = 0; i <= 10; i++) {
			if(stopSpawning) break;
			yield return new WaitForSeconds (2);
			float rand = Random.Range(-26, -14);
			GameObject newEnemy = (GameObject)Instantiate(og, new Vector3(rand, -3, 0), Quaternion.identity);
			newEnemy.AddComponent<EnemyState>();
			newEnemy.transform.Rotate(340, 180, 0);
		}
	}


	
//	void OnCollisionEnter(Collision collider){
//		print ("Enter");
//		colliding = true;
//	}
//	
//	void OnCollisionStay(Collision collision){
//		//		print("Collision");
//		collision2 = collision;
//	}
//
//	void OnCollisionLeave(Collision collider){
//		colliding = false;
//	}
}







