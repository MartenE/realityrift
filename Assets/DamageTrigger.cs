﻿using UnityEngine;
using System.Collections;

public class DamageTrigger : MonoBehaviour {

	GUITexture text;
	// Use this for initialization
	void Start () {
		text = GetComponent<GUITexture> ();

		var color = text.color;
		color.a = 0f;
		text.color = color;
//		StartCoroutine(FlashWhenHit ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Fade ( float start, float end, float length,  GameObject currentObject ) { //define Fade parmeters 
		if (text.color.a == start){
			for (float i = 0.0f; i < 1.0f; i += Time.deltaTime*(1/length)) { //for the length of time 
				var color = text.color;
					color.a = Mathf.Lerp(start, end, i); //lerp the value of the transparency from the start value to the end value in equal increments yield; 
					text.color = color;
				color.a = end; // ensure the fade is completely finished (because lerp doesn't always end on an exact value) 
				text.color = color;
			} //end for
			
		} //end if
		
	} //end Fade
	
	public IEnumerator FlashWhenHit (){
		print ("FLASH");
		Fade (0f, 0.2f, 0.5f, this.gameObject); 
		yield return new WaitForSeconds (.1f); 
		Fade (0.2f, 0f, 0.5f, this.gameObject); 
	} 
}
