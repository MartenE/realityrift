﻿using UnityEngine;
using System.Collections;

public class TextureHandler : MonoBehaviour {
//	Sprite sprite;
	
	public Transform target;
	public float speed;

	public Vector3 posA;
	public Vector3 posB;
	public Vector3 posC;

	public bool moveRight;
	public bool moveUp;
	// Use this for initialization
	void Start () {

		speed = .7f;
		moveRight = true;
		moveUp = false;

		posA = new Vector3(-23,3,-7);
		posB = new Vector3 (-15, 6,-7);
		posC = new Vector3 (-19,7,-7);
	}


	void Update() {
//		transform.LookAt (new Vector3 (-20, 5, -10), Vector3.up);


		float step = speed * Time.deltaTime;

		if (moveRight == true)
			transform.position = Vector3.MoveTowards(transform.position, posB, step);


		if (transform.position == posB) {
			moveRight = false;
			moveUp = true;
		}

		if (transform.position == posA)
			moveRight = true;

		if (moveUp == true)
			transform.position = Vector3.MoveTowards (transform.position, posC, step);

		if (transform.position == posC) {
			moveUp = false;
			moveRight = false;
		}

		if(moveUp == false && moveRight == false)
			transform.position = Vector3.MoveTowards (transform.position, posA, step);
	}
}
