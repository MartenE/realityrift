﻿using UnityEngine;
using System.Collections;

public class HealthController : MonoBehaviour {
	public int health;
	GameObject damageTexture;
	MeshRenderer loseText;
	DamageTrigger trigger;
	bool canHurt;
	bool isColliding;
	bool losing;
	GUITexture text;

	// Use this for initialization
	void Start () {
		health = 5;
		damageTexture = GameObject.Find ("Damage");
		loseText = GameObject.Find ("Lost").GetComponent<MeshRenderer>();
		text = damageTexture.GetComponent<GUITexture> ();

		trigger = damageTexture.GetComponent<DamageTrigger> ();
		canHurt = true;
		isColliding = false;
		losing = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(GameObject.Find("Enemy(Clone)") == null){
			isColliding = false;
//			canHurt = false;
		}


		if (health <= 0 && !losing){

			print ("You Lost! :@");
			AudioClip deathSound = Resources.Load("Sound/dying", typeof(AudioClip))as AudioClip;
			AudioSource source = GetComponent<AudioSource>();
			source.clip = deathSound;
			source.Play();

			if (GameObject.Find("Reticle") != null)
			GameObject.Find("Reticle").GetComponent<Reticle>().stopSpawning = true;

			text.color = Color.black;
			var color = text.color;
			color.a = 1f;
			text.color = color;

			loseText.enabled = true;
			losing = true;
		}

		if (health > 0 && isColliding && canHurt) {
			StartCoroutine(takeDamage());
		}

//		if (Input.GetKeyDown (KeyCode.Return)) {
//			WebCamTexture texture = GameObject.Find("Background Image").GetComponent<WebCamTexture>();
//			while (texture!=null && texture.isPlaying)
//			{
//				Debug.Log("is still playing");
//				texture.Stop();
//				texture=null;
//				break;
//			}
//			Debug.Log("stoped playing");
//			Application.LoadLevel(Application.loadedLevel);
//		}  
	}

	IEnumerator takeDamage(){
		StartCoroutine(trigger.FlashWhenHit ());
		health -= 1;
		GetComponent<AudioSource> ().Play ();

		canHurt = false;
		yield return new WaitForSeconds (1);
		canHurt = true;
	}

	void OnCollisionEnter(Collision collider){
		print ("Enter");
	}

	void OnCollisionStay(Collision collision){
		if (collision.collider.tag == "Enemy") {
			collision.collider.GetComponent<EnemyState>().speed = 0f;
			print("Colliding");
			isColliding = true;
		}
	}
	void OnCollisionLeave(Collision collider){
		isColliding = false;
	}
}
