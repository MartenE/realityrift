﻿using UnityEngine;
using System.Collections;

public class EnemyState : MonoBehaviour {

	Animator anim;
	Transform target;
	public float speed;
	public int health;
	AudioSource source;
	bool canPlay;
	bool canDie;

	// Use this for initialization
	void Start () {
//		anim = GetComponent<Animator> (); 
//		anim.SetBool ("IsWalking", true);
		speed = .3f;
		health = 2;
		canPlay = true;
		canDie = true;

		source = GetComponent<AudioSource> ();
//		target.position = new Vector3 (-20, -3, -10);
	}
	
	// Update is called once per frame
	void Update () {

		float step = speed * Time.deltaTime;

		transform.position = Vector3.MoveTowards(transform.position, new Vector3 (-20, 5, -10), step);

		if (this.health <= 0) {
			Destroy(this.gameObject);
		}

		if (GameObject.Find ("Reticle").GetComponent<Reticle> ().stopSpawning)
			Destroy (this.gameObject);

		if (canPlay && !source.isPlaying)
			StartCoroutine (playSound());
	}

	IEnumerator playSound(){
		source.Play ();
		canPlay = false;
		yield return new WaitForSeconds (4);
		canPlay = true;
	}
}
